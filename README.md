# README #

Welcome to the FFWD repository. FFWD is a latency-aware load shedding framework that exploits performance degradation techniques to adapt the throughput of the application to the size of the input, allowing the system to have a fast and reliable response time in case of overloading.

FFWD exploits an heuristic controller formulation to keep bounded the response time and uses domain-specific policies to enhance accuracy in output metrics estimation.

## Getting started ##

### Prerequisites ###
* Java 8
* Maven https://maven.apache.org/download.cgi
* RabbitMQ Server https://www.rabbitmq.com/download.html
* Stanford Core NLP http://stanfordnlp.github.io/CoreNLP/

### Setup instructions ###

Clone the repository

Copy stanford-corenlp-3.6.0-models.jar and stanford-corenlp-3.6.0-javadoc.jar files from the Stanford Core NLP package into the root directory of the project

In the root directory of the project create a local maven repository typing:
```
#!bash
mvn install:install-file -Dfile=stanford-corenlp-3.6.0-models.jar -DgroupId=edu.stanford.nlp -DartifactId=model -Dversion=3.6.0 -Dpackaging=jar -DgeneratePom=true
```

Modify the pom.xml file adding the stanford nlp model dependency:

```
#!xml

<dependency>
    <groupId>edu.stanford.nlp</groupId>
    <artifactId>model</artifactId>
    <version>3.6.0</version>
</dependency>
```

### Twitter Streaming API setup ###

In order to work with streaming tweets, in the Settings.java file the Twitter API settings should be inserted. Please refer to https://apps.twitter.com to create a Twitter streaming application and to obtain access and consumer credentials.

### Test setup ###

To test the application with the recorded tweets, download the tweets file at https://www.dropbox.com/s/as5cz3q4kan08ku/tweets.txt?dl=0 and copy it in the jar directory for Maven setup or in the project directory for Eclipse setup.

### Compilation instructions ###

In the root folder of the project type:

```
#!bash

mvn assembly:assembly -DdescriptorId=jar-with-dependencies
```
This command will download all the dependencies, compile the project and assemble it in a unique portable jar.

## Run code and tests ##

Start the RabbitMQ server

Run the consumer with the command:

```
#!bash

java -classpath ffwd-0.0.1-SNAPSHOT-jar-with-dependencies.jar it.polimi.necst.ffwd.consumer.SentimentMain QOS LAMBDA_AVG POLICY
```
where **QOS** is the output emission rate in seconds, **LAMBDA_AVG** is a flag for using the arrival rate average (0 disabled, 1 enabled) and **POLICY** is an integer used to select the load shedding policy (0 baseline, 1 Priority, 2 Fair).

Run the producer with the command:

```
#!bash

java -classpath ffwd-0.0.1-SNAPSHOT-jar-with-dependencies.jar it.polimi.necst.ffwd.producer.ProducerMonitor ENV options
```
The env parameter selects the producer mode:

* real: the producer sends to the queue twitter API events
* fake: the producer sends to the queue a fixed tweet
* test: the producer sends to the queue the recorded tweets
* demo: the producer sends to the queue the demo trace from the recorded tweets

**real** and **demo** don't need further parameters.

**fake** and **test** requires as parameters:

* maximum number of events per second
* number of seconds for the run
* ramp flag (0 for step input, 1 for increasing input rate)
* number of repeated runs

## Useful Links ##

* Tweet collection (4,6GB) https://www.dropbox.com/s/as5cz3q4kan08ku/tweets.txt?dl=0

## Authors ##

* Rolando Brondolin
* Matteo Ferroni
* Marco D. Santambrogio

## Licence ##
Copyright (c) 2016 Politecnico di Milano

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
