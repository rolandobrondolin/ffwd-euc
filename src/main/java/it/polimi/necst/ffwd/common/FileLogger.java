package it.polimi.necst.ffwd.common;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class FileLogger {
	String path;
	
	BufferedWriter bwGeneral;
	BufferedWriter bwController;
	BufferedWriter bwAccuracy;
	
	public FileLogger(){
		path = System.getProperty("user.dir");
	}
	
	public void open() throws IOException{
		
		File file = new File(path + File.separator + "log.txt");
		File fileController = new File(path + File.separator + "controller.txt");
		File fileAccuracy = new File(path + File.separator + "accuracy.txt");
		// if file doesnt exists, then create it
		if (!file.exists()) {
			file.createNewFile();
		}
		if (!fileController.exists()) {
			fileController.createNewFile();
		}
		if (!fileAccuracy.exists()) {
			fileAccuracy.createNewFile();
		}
		
		FileWriter fw = new FileWriter(file.getAbsoluteFile());
		bwGeneral = new BufferedWriter(fw);
		fw = new FileWriter(fileController.getAbsoluteFile());
		bwController = new BufferedWriter(fw);
		fw = new FileWriter(fileAccuracy.getAbsoluteFile());
		bwAccuracy = new BufferedWriter(fw);
	}
	
	public void println(String line){
		if(bwGeneral!=null){
			if(Settings.verbose){
				System.out.println(line);
			}
			if(Settings.log){
				try {
					bwGeneral.write(line);
					bwGeneral.newLine();
					bwGeneral.flush();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public void print(String input){
		if(bwGeneral!=null){
			if(Settings.verbose){
				System.out.print(input);
			}
			if(Settings.log){
				try {
					bwGeneral.write(input);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public void printToControllerFile(String line){
		if(bwController!=null && Settings.collectResults){
			try {
				bwController.write(line);
				bwController.newLine();
				bwController.flush();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void printToAccuracyFile(String line){
		if(bwAccuracy!=null && Settings.collectResults){
			try {
				bwAccuracy.write(line);
				bwAccuracy.newLine();
				bwAccuracy.flush();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	
	public void close(){
		try {
			bwGeneral.flush();
			bwGeneral.close();
			bwController.flush();
			bwController.close();
			bwAccuracy.flush();
			bwAccuracy.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
}
