package it.polimi.necst.ffwd.consumer;

public class CategoryContribution {
	private String key;
	private float count;
	private float drop;
	private float responseTime;
	
	public CategoryContribution(String key, float count, float drop, float responseTime) {
		super();
		this.key = key;
		this.count = count;
		this.drop = drop;
		this.responseTime = responseTime;
	}


	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public float getCount() {
		return count;
	}

	public void setCount(float count) {
		this.count = count;
	}

	public float getDrop() {
		return drop;
	}

	public void setDrop(float drop) {
		this.drop = drop;
	}
	
	public float getTotal(){
		return count + drop;
	}

	public float getResponseTime() {
		return responseTime;
	}


	public void setResponseTime(float responseTime) {
		this.responseTime = responseTime;
	}
	
	
}
