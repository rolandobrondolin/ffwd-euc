package it.polimi.necst.ffwd.consumer;

import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Consumer;

import it.polimi.necst.ffwd.common.FileLogger;
import it.polimi.necst.ffwd.common.Settings;
import it.polimi.necst.ffwd.producer.Event;
import it.polimi.necst.ffwd.shedder.FairHashtagPolicy;
import it.polimi.necst.ffwd.shedder.FairKeywordPolicy;
import it.polimi.necst.ffwd.shedder.BaselinePolicy;
import it.polimi.necst.ffwd.shedder.LoadController;
import it.polimi.necst.ffwd.shedder.LoadFilter;
import it.polimi.necst.ffwd.shedder.PriorityBasedPolicy;
import it.polimi.necst.ffwd.shedder.SheddingPolicy;


public class SentimentMain {
	
	public static void main(String[] argv) throws Exception {
		int qos = Settings.qos;
		boolean useLambdaAverage = Settings.useLambdaAverage;
		int policy = Settings.policy;
		if(argv.length>0){
			qos = Integer.parseInt(argv[0]);
			useLambdaAverage = Integer.parseInt(argv[1]) == 0 ? false : true;
			policy = Integer.parseInt(argv[2]);
		}
		
		runSentiment(qos, useLambdaAverage, policy);
	}
	
	public static void runSentiment(int qos, boolean lambdaAverage, int policyNumber) throws Exception{
		int stepCount = 0;
		//busy wait flag for workers
		AtomicBoolean stopWorkers = new AtomicBoolean();
		stopWorkers.set(false);
		
		//Create internal queue for asynch aggregation
		BlockingQueue<Event> aggregateQueue = new LinkedBlockingQueue<>(10000);
		
		//open logger for clean print and offline analysis
		FileLogger logger = new FileLogger();
		try{
			logger.open();
		}
		catch (IOException e){
			e.printStackTrace();
			return;
		}
		//Connect to rabbit
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("localhost");
		Connection connection = factory.newConnection();
		//Create channel list for consumers
		ArrayList<Channel> channels = new ArrayList<>();
		
		
		//Create aggregator thread
		SentimentAggregator sAggregator = new SentimentAggregator(channels, logger, aggregateQueue, qos);
		sAggregator.start();
		
		//setup control and shedding scheme
		LoadFilter filter = new LoadFilter(Settings.sendToBatch);
		
		//switch policy depending on settings or user input
		SheddingPolicy policy;
		switch(policyNumber){
		case 0: 
			policy = new BaselinePolicy(sAggregator, filter, logger);
			break;
		case 1:
			policy = new PriorityBasedPolicy(sAggregator, filter, logger);
			break;
		case 2:
			policy = new FairKeywordPolicy(sAggregator, filter, logger);
			break;
		case 3:
			policy = new FairHashtagPolicy(sAggregator, filter, logger);
			break;
		default:
			policy = new BaselinePolicy(sAggregator, filter, logger);
		}
		filter.setPolicy(policy);
		LoadController lController = new LoadController(qos, filter, policy, logger);
		
		//create consumers (aka event capture)
		for(int i=0; i<Settings.consumerThreads; i++){
			Channel channel = connection.createChannel();
			channels.add(channel);
			channel.queueDeclare(Settings.REAL_TIME_QUEUE, true, false, false, null);
			//dropping channel
			Channel dropCh = null;
			
			if(Settings.sendToBatch){
				dropCh = connection.createChannel();
				dropCh.queueDeclare(SheddingPolicy.BATCH_QUEUE, true, false, false, null);
			}
			
			channel.basicQos(1);
			Consumer queueConsumer = new EventCapture(channel, dropCh, filter, aggregateQueue, stopWorkers);
			channel.basicConsume(Settings.REAL_TIME_QUEUE, false, queueConsumer);
		}
		
		System.out.println("Waiting for messages. To exit press CTRL+C");
		
		// Control loop
		while(true){
			long timestampControlStep = System.currentTimeMillis();

			//send stop signal to workers
			stopWorkers.set(true);
			
			//collect data from aggregation phase and provide it to controller
			float meanResponseTime = sAggregator.getPerformance();
			long count = sAggregator.getCount();
			long drop = sAggregator.getDrop();
			//get queue length
			long queue = channels.get(1).messageCount(Settings.REAL_TIME_QUEUE);
			logger.printToControllerFile(stepCount + " " + count + " " + drop + " " + sAggregator.getInsidePlan() + " " + sAggregator.getOutsidePlan() + " " + meanResponseTime + " " + (count+drop) + " " + queue);
			
			//call load controller to determine the new system throughput
			lController.computeSheddingCoefficients(meanResponseTime, count, drop, queue, lambdaAverage);
			
			//restart work
			stopWorkers.set(false);
			
			stepCount++;
			
			//wait until next control period
			long elapsed = System.currentTimeMillis() - timestampControlStep;
			if(1000 - elapsed > 0){
				Thread.sleep(1000 - elapsed);
			}
			
		}
	}
	
	
}
