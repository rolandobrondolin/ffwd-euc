package it.polimi.necst.ffwd.consumer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;

import com.rabbitmq.client.Channel;

import it.polimi.necst.ffwd.common.FileLogger;
import it.polimi.necst.ffwd.common.Settings;
import it.polimi.necst.ffwd.producer.Event;

public class SentimentAggregator extends Thread{

	private long stepCount;
	private long count;
	private long drop;
	private long[] scores;
	private long responseTime;
	
	private long controllerCount;
	private long controllerDrop;
	private long controllerResponseTime;
	
	private ArrayList<Channel> channels;
	private FileLogger logger;
	
	private long insidePlan;
	private long outsidePlan;
	
	private ConcurrentHashMap<String, KeywordItem> keywordTable;
	private ConcurrentHashMap<String, HashtagItem> globalHashtagTable;
	
	private BlockingQueue<Event> aggregateQueue;
	
	private long ts_internal;
	private int qos;
	
	private long controllerInsidePlan;
	private long controllerOutsidePlan;
	
	public SentimentAggregator(ArrayList<Channel> channels, FileLogger logger, BlockingQueue<Event> aggregateQueue, int qos){
		stepCount = 0;
		count = 0;
		drop = 0;
		
		controllerCount=0;
		controllerDrop=0;
		controllerResponseTime = 0;
		controllerInsidePlan = 0;
		controllerOutsidePlan = 0;
		insidePlan = 0;
		outsidePlan = 0;
		scores = new long[5];
		responseTime = 0;
		this.channels = channels;
		keywordTable = new ConcurrentHashMap<String, KeywordItem>();
		globalHashtagTable = new ConcurrentHashMap<String, HashtagItem>();
				
		this.logger = logger;
		this.aggregateQueue = aggregateQueue;
		
		this.ts_internal = 0;
		this.qos = qos;
		
		//collect static group results
		if(Settings.mapMetrics){
			for(int i=0; i<4; i++){
				KeywordItem newKeyword = new KeywordItem();
				keywordTable.put(Integer.toString(i), newKeyword);
			}
		}
	}
	
	@Override
	public void run(){
		while(true){
			try {
				//pick a computed event from the internal queue
				Event evt = aggregateQueue.take();
				//if it is time to emit, emit metrics (to std_out until now)
				if(ts_internal == 0){
					ts_internal = evt.getTs();
				}
				if(evt.getTs() - ts_internal > 1000*qos){
					ts_internal = evt.getTs();
					this.emitScores();
				}
				
				//check if the event was dropped or not
				if(evt.isDropped()){
					//account dropped stuff
					this.addDroppedSentiment(evt);
				}
				else{
					//update tables in case of new keywords or hashtags
					this.tableUpdate(evt);
					this.addSentiment(evt);
				}
				//account if the shedding plan caught the event
				this.planAccuracyUpdate(evt);
				
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void tableUpdate(Event message){
		//add stuff for keywordTable when data are not grouped
		//dynamic update supported only with single keywords
		if(!Settings.mapMetrics){
			for(String s: message.getKeywords()){
				if(!keywordTable.containsKey(s)){
					KeywordItem newKeyword = new KeywordItem();
					
					for(String h: message.getHashTags()){
						HashtagItem newHashtag = new HashtagItem();
						newKeyword.getHashtagTable().put(h, newHashtag);
					}
					keywordTable.put(s, newKeyword);
					
				}
				else{
					KeywordItem keyword = keywordTable.get(s);
					
					for(String h: message.getHashTags()){
						if(!keyword.getHashtagTable().containsKey(h)){
							HashtagItem newHashtag = new HashtagItem();
							keyword.getHashtagTable().put(h, newHashtag);
						}
					}
				}
			}
		}
		//add stuff for hashtagTable
		for(String h: message.getHashTags()){
			if(!globalHashtagTable.containsKey(h)){
				HashtagItem globalHashtag = new HashtagItem();
				globalHashtagTable.put(h, globalHashtag);
			}
		}
		
	}
	
	public void planAccuracyUpdate(Event message){
		if(message.isInsidePlan()){
			insidePlan++;
			controllerInsidePlan++;
		}
		else{
			outsidePlan++;
			controllerOutsidePlan++;
		}
	}
	
	public void addDroppedSentiment(Event message){
		drop++;
		controllerDrop++;
		long currentTs = System.currentTimeMillis();
		responseTime = responseTime + currentTs - message.getTs();
		controllerResponseTime = controllerResponseTime + currentTs - message.getTs();
		
		
		if(Settings.mapMetrics){
			//add drop for the grouped metrics
			for(String s : message.getKeywords()){
				for(int i =0; i<4;i++){
					for(int j=0; j<7;j++){
						if(Settings.mapping[i][j].equals(s)){
							keywordTable.get(Integer.toString(i)).putDrop(currentTs - message.getTs(), 1);
						}
					}
				}
			}
		}
		else{
			for(String s : message.getKeywords()){
				if(keywordTable.containsKey(s)){
					keywordTable.get(s).putDrop(currentTs - message.getTs(), message.getKeywords().size());
				}
			}
		}
		
		for(String s: message.getHashTags()){
			if(globalHashtagTable.containsKey(s)){
				globalHashtagTable.get(s).putDrop(currentTs - message.getTs(), message.getHashTags().size());
			}
		}
	}
	
	public void addSentiment(Event message){
		scores[message.getSentimentScore()]++;
		count++;
		controllerCount++;
		long currentTs = System.currentTimeMillis();
		responseTime = responseTime + currentTs - message.getTs();
		controllerResponseTime = controllerResponseTime + currentTs - message.getTs();
		
		
		if(Settings.mapMetrics){
			//add score for the grouped metrics
			for(String s : message.getKeywords()){
				for(int i = 0; i<4;i++){
					for(int j=0; j<7;j++){
						if(Settings.mapping[i][j].equals(s)){
							keywordTable.get(Integer.toString(i)).putScore(message.getSentimentScore(), currentTs - message.getTs(), message.getKeywords().size());
						}
					}
				}
			}
		}
		else{
			//account table stats for keywords
			for(String s: message.getKeywords()){
				keywordTable.get(s).putScore(message.getSentimentScore(), currentTs - message.getTs(), message.getKeywords().size());
				for(String h: message.getHashTags()){
					keywordTable.get(s).putHashtagScore(h, message.getSentimentScore(), currentTs - message.getTs());
				}
			}
		}
		//account table stats for hashtags
		for(String h: message.getHashTags()){
			globalHashtagTable.get(h).putScore(message.getSentimentScore(), currentTs - message.getTs(), message.getHashTags().size());
		}
	}
	
	
	//Functions for the controller
	//Executed when the thread is waiting for the blocking queue
	public void emitScores(){
		stepCount++;
		
		if(count>0){
			float insidePct = ((float)(insidePlan)/(float)(count+drop))*100;
			float outsidePct = ((float)(outsidePlan)/(float)(count+drop))*100;
			logger.println("Count: " + count);
			logger.println("Drop: " + drop);
			logger.println("Inside: "+ insidePlan + " " + insidePct +  " Outside: " + outsidePlan + " " + outsidePct);
			
			try {
				String s = "";
				for(Channel c: channels){
					s = s + (c.messageCount(Settings.REAL_TIME_QUEUE) + " ");
				}
				String logLine = "Queue Size: ";
				logger.println(logLine + s);
								
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			logger.println("Mean response time: " + (((float)responseTime)/((float)(count+drop)))/1000);
			
			
			//rescale general events!
			long total = count + drop;
			//score: count == biggerScore : total
			//bigscore = total*score/count;
			for(int i=0; i<scores.length; i++ ){
				if(scores[i]!=0 && count !=0){
					scores[i] = (total*scores[i])/count;
				}
			}
			
			String buffer = stepCount + " " + "-1" + " " +  "general" + " ";
			for(int i=0; i<5; i++){
				buffer = buffer + scores[i] + " " + (((float) scores[i])/((float) count+drop))*100 + " ";
				
				logger.println(i + " " + scores[i] + " " + ( ((float) scores[i])/((float) count+drop))*100 + "%;" );
			}
			buffer = buffer + count + " " + drop;
			//emit general stats
			logger.printToAccuracyFile(buffer);
			
			
			//flush table stats after emission
			for(Entry<String, KeywordItem> k : keywordTable.entrySet()){
				//emit keyword stats before flush of the item
				//rescale values depending on drop stats
				k.getValue().rescale();
				buffer = k.getValue().getAccuracyString(stepCount, k.getKey());
				
				if(k.getValue().getCount()>0 || k.getValue().getDrop()>0){
					logger.printToAccuracyFile(buffer);
				}
				//flush item
				k.getValue().flushItem();
			}
			
			for(Entry<String, HashtagItem> h : globalHashtagTable.entrySet()){
				//emit hashtag stats before flush of the item
				//rescale values depending on drop stats
				h.getValue().rescale();
				buffer = h.getValue().getAccuracyString(stepCount, h.getKey());
				
				if(h.getValue().getCount()>0 || h.getValue().getDrop()>0){
					logger.printToAccuracyFile(buffer);
				}
				
				//flush item
				h.getValue().flushItem();
			}
			
		}
		else{
			logger.println("Count: " + count);
			try {
				String s = "";
				for(Channel c: channels){
					s = s + (c.messageCount(Settings.REAL_TIME_QUEUE) + " ");
				}
				String logLine = "Queue Size: ";
				logger.println(logLine + s);
								
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			logger.printToAccuracyFile(stepCount + " " + "-1" + " " +  "general" + " " + 0 + " " + 0 + " " + 0 + " " + 0 + " " + 0 + " " + 0 + " " + 0 + " " + 0 + " " + 0 + " " + 0 + " " + 0 + " " + 0);
		}
		
		//reset counters after emission
		scores = new long[5];
		count = 0;
		drop = 0;
		responseTime = 0;
		insidePlan = 0;
		outsidePlan = 0;
		
	}

	public float getPerformance(){
		float ret = (((float)controllerResponseTime)/((float)(controllerCount+controllerDrop)))/1000;
		controllerResponseTime = 0;
		return ret;
	}
	public long getCount(){
		long ret = controllerCount;
		controllerCount=0;
		return ret;
	}
	public long getDrop(){
		long ret = controllerDrop;
		controllerDrop=0;
		return ret;
	}
	public long getInsidePlan(){
		long ret = controllerInsidePlan;
		controllerInsidePlan = 0;
		return ret;
	}
	public long getOutsidePlan(){
		long ret = controllerOutsidePlan;
		controllerOutsidePlan = 0;
		return ret;
	}

	public ArrayList<CategoryContribution> getKeywords(){
		ArrayList<CategoryContribution> ret = new ArrayList<>();
		
		for(Entry<String, KeywordItem> t : keywordTable.entrySet()){
			//empty stat keywords falls down in the standard drop scenario
			if(t.getValue().get_old_weighted_count() != 0){
				ret.add(new CategoryContribution(t.getKey(),t.getValue().get_old_weighted_count(), t.getValue().get_old_weighted_drop(), t.getValue().getPerformance()));
			}
		}
		return ret;
	}

	public ArrayList<CategoryContribution> getHashtags(){
		ArrayList<CategoryContribution> ret = new ArrayList<>();
		
		for(Entry<String, HashtagItem> t : globalHashtagTable.entrySet()){
			//empty stat keywords falls down in the standard drop scenario
			if(t.getValue().get_old_weighted_count() != 0){
				ret.add(new CategoryContribution(t.getKey(), t.getValue().get_old_weighted_count(), t.getValue().get_old_weighted_drop(), t.getValue().getPerformance()));
			}
		}
		
		return ret;
	}
}
