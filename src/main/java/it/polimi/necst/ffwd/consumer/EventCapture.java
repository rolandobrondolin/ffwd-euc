package it.polimi.necst.ffwd.consumer;

import java.io.EOFException;
import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.AlreadyClosedException;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.MessageProperties;

import it.polimi.necst.ffwd.common.Settings;
import it.polimi.necst.ffwd.producer.Event;
import it.polimi.necst.ffwd.producer.Serializer;
import it.polimi.necst.ffwd.shedder.LoadFilter;
import it.polimi.necst.ffwd.shedder.SheddingPolicy;

public class EventCapture extends DefaultConsumer{
	
	private SentimentAnalyzer sAnalyzer;

	private LoadFilter filter;
	private BlockingQueue<Event> aggregateQueue;
	private Channel dropCh;
	
	private AtomicBoolean stop;
	
	public EventCapture(Channel channel, Channel dropCh, LoadFilter filter, BlockingQueue<Event> aggregateQueue, AtomicBoolean stopWorker) {
		super(channel);
		sAnalyzer = new SentimentAnalyzer();
		this.filter = filter;
		this.dropCh = dropCh;
		this.aggregateQueue = aggregateQueue;
		this.stop = stopWorker;
	}
	
	@Override
	public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
		
		//Check if it is time to stop for a while
		while(stop.get());
		
		Event message;
		try {
			message = Serializer.deserialize(body);
			processMessage(message);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (EOFException e){
			e.printStackTrace();
		} catch (Exception e){
			e.printStackTrace();
		}
		try{
			getChannel().basicAck(envelope.getDeliveryTag(), false);
		}
		catch (AlreadyClosedException e){
			e.printStackTrace();
		}
		

	}
	
	private void processMessage(Event message){
		//shedding part of the process
		//check against filter if we must drop event or compute it
		if(filter.accept(message)){
			try {
				//do analysis stuff
				sAnalyzer.doSentiment(message);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		else{
			//send the event to batch queue if requested
			if(filter.sendToBatch()){
				try {
					dropCh.basicPublish("", SheddingPolicy.BATCH_QUEUE, MessageProperties.PERSISTENT_TEXT_PLAIN, Serializer.serialize(message));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		//Send the message to the aggregator for final accounts (either if dropped or not)
		try {
			aggregateQueue.put(message);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public long getQueueSize() throws IOException{
		return getChannel().messageCount(Settings.REAL_TIME_QUEUE);
	}
}
