package it.polimi.necst.ffwd.producer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeoutException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.common.collect.Lists;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;
import com.twitter.hbc.ClientBuilder;
import com.twitter.hbc.core.Client;
import com.twitter.hbc.core.Constants;
import com.twitter.hbc.core.Constants.FilterLevel;
import com.twitter.hbc.core.endpoint.StatusesFilterEndpoint;
import com.twitter.hbc.core.processor.StringDelimitedProcessor;
import com.twitter.hbc.httpclient.auth.Authentication;
import com.twitter.hbc.httpclient.auth.OAuth1;

import it.polimi.necst.ffwd.common.Settings;

public class RealTimeThread extends Thread{

	ArrayList<String> keywords;
	boolean live;
	private BlockingQueue<String> queue;
    private StatusesFilterEndpoint endpoint;
    private Client client;
    private Connection connection;
    Channel channel;
    private long ts;
    private int count;
	
	public RealTimeThread(ArrayList<String> keywords){
		this.keywords = keywords;
		live = true;
		
		queue = new LinkedBlockingQueue<String>(10000);
	    endpoint = new StatusesFilterEndpoint();
	    ts = System.currentTimeMillis();
	    count = 0;
	}
	
	public void run() {

		//Connect rabbit queue
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("localhost");
		try {
			connection = factory.newConnection();
			channel = connection.createChannel();
			channel.queueDeclare(Settings.REAL_TIME_QUEUE, true, false, false, null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		//connect to twitter streaming API
		endpoint.trackTerms(keywords);
		endpoint.filterLevel(FilterLevel.None);
		endpoint.languages(Lists.newArrayList(Settings.tweetLanguage));
		Authentication auth = new OAuth1(Settings.consumerKey, Settings.consumerSecret, Settings.accessToken, Settings.accessSecret);

		client = new ClientBuilder().hosts(Constants.STREAM_HOST).endpoint(endpoint).authentication(auth).processor(new StringDelimitedProcessor(queue)).build();
		client.connect();
		
		//get messages and send them to the rabbit queue
	    while(live){
	    	JSONObject obj = this.getMessage();
			if(obj!=null){
	    		Event msg = new Event(obj);
	    		msg.setText(this.getText(obj));
	    		msg.setHashTags(this.getHashTags(obj));
	    		setKeywords(msg);
	    		try {
	    			byte[] payload = Serializer.serialize(msg);
	    			if(payload!=null){
						channel.basicPublish("", Settings.REAL_TIME_QUEUE, MessageProperties.PERSISTENT_TEXT_PLAIN, payload);
	    			}
				} catch (IOException e) {
					e.printStackTrace();
				}
	    		
	    		count++;
	    		if(msg.getTs() - ts>1000){
	    			System.out.println("Monitor: " + ((float)ts)/1000F + " " + count);
	    			ts = msg.getTs();
	    			count = 0;
	    		}
	    	}
	    	
	    }
	    
	    //closing code
	    if(client!=null){
	    	client.stop();
	    }
		
		try {
			if(channel!=null){
				channel.close();
			}
			if(connection!=null){
				connection.close();
			}
		} catch (IOException e) {

		} catch (TimeoutException e) {

		}
		return;
	}
	
	
	public void kill(){
		live = false;
	}
	
	private JSONObject getMessage(){
		try {
			String jsonTweet = queue.take();
			JSONObject obj = new JSONObject(jsonTweet);
			int i = 0;
			try{
				i = obj.getJSONObject("limit").getInt("track");
			}
			catch (JSONException e){
				
			}
			if(i!=0){
				System.out.println(i);
			}
			return obj;
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if(client!=null){
				client.stop();
			}
			return null;
		}
	}
	
	private String getText(JSONObject tweet){
		try{
			String text = tweet.getString("text");
			return text;
		}
		catch (Exception e){
			return "";
		}	
	}
	
	//This is potentially time consuming
	private void setKeywords(Event message){
		for(String s: keywords){
			boolean inside = Arrays.asList(message.getText().toLowerCase().split(" ")).contains(s.toLowerCase());
			if(inside){
				message.getKeywords().add(s);
			}
		}
	}
	
	private ArrayList<String> getHashTags(JSONObject tweet){
		try{
			ArrayList<String> tags = new ArrayList<String>();
			JSONArray hashtags = tweet.getJSONObject("entities").getJSONArray("hashtags");
			for(int i=0; i<hashtags.length(); i++){
				tags.add(hashtags.getJSONObject(i).getString("text"));
			}
			return tags;
		}
		catch (Exception e){
			return new ArrayList<String>();
		}
	}
	
}
