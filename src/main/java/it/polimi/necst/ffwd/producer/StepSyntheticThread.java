package it.polimi.necst.ffwd.producer;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import org.json.JSONObject;

import com.google.common.collect.Lists;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;

import it.polimi.necst.ffwd.common.Settings;

public class StepSyntheticThread extends Thread{
	
	private int numEvents;
	private int iterations;
	
	private static final String message = "tweet with a long beutiful phrase. The second one instead is longer but really sad. The third one is neutral";
	private static final String tweet = "{'synthetic_tweet': 1}";
	
    private Connection connection;
    Channel channel;
	
	public StepSyntheticThread(int numEvents, int iterations){
		this.numEvents = numEvents;
		this.iterations = iterations;
	}
	
	@Override
	public void run(){
		//Connect to rabbit queue
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("localhost");
		try {
			connection = factory.newConnection();
			channel = connection.createChannel();
			channel.queueDeclare(Settings.REAL_TIME_QUEUE, true, false, false, null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		
		
		Event msg = new Event(new JSONObject(tweet));
		msg.getKeywords().add("message");
		msg.setText(message);
		msg.setHashTags(Lists.newArrayList("message1","message2"));
		
		for(int i=0; i<iterations; i++){
			long ts = System.currentTimeMillis();
			for(int j= 0; j<numEvents; j++){
				try {

					msg.setTs(System.currentTimeMillis());
	    			byte[] payload = Serializer.serialize(msg);
	    			if(payload!=null){
						channel.basicPublish("", Settings.REAL_TIME_QUEUE, MessageProperties.PERSISTENT_TEXT_PLAIN, payload);
	    			}
					
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			try {
				System.out.println(i + " " + iterations + " " + numEvents);
				long sleeptime = 1000 - (long) (((float)(System.currentTimeMillis() - ts))/1000);
				Thread.sleep(sleeptime);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

}
