package it.polimi.necst.ffwd.producer;

import java.util.Scanner;

import com.google.common.collect.Lists;

import it.polimi.necst.ffwd.common.Settings;

public class ProducerMonitor {
	
	@SuppressWarnings("unused")
	public static void main(String[] argv) throws Exception {
		//input parameters for tests
		if(argv.length>0){
			//take input parameters
			String env = argv[0];
			
			if(env.equals("fake")){
				String evt = argv[1];
				String time = argv[2];
				String staticSend = argv[3];
				String run = argv[4];
				
				int iRun = Integer.parseInt(run);
				int iStaticSend = Integer.parseInt(staticSend);
				int iTime = Integer.parseInt(time);
				int iEvt = Integer.parseInt(evt);
				if(iStaticSend == 0){
					StepSyntheticThread th;
					for(int i = 0; i<iRun; i++){
						th = new StepSyntheticThread(iEvt, iTime);
						th.start();
						th.join();
						Thread.sleep(20000);
					}
				}
				else{
					RampSyntheticThread rt = new RampSyntheticThread(iStaticSend, iEvt, iTime);
					rt.start();
					rt.join();
					Thread.sleep(30000);
				}
			}
			if(env.equals("test")){
				String evt = argv[1];
				String time = argv[2];
				String staticSend = argv[3];
				String run = argv[4];
				
				int iRun = Integer.parseInt(run);
				int iStaticSend = Integer.parseInt(staticSend);
				int iTime = Integer.parseInt(time);
				int iEvt = Integer.parseInt(evt);
				
				if(iStaticSend == 0){
					StepRecordedThread th;
					for(int i = 0; i<iRun; i++){
						th = new StepRecordedThread("tweets.txt", iTime, iEvt);
						th.start();
						th.join();
						Thread.sleep(20000);
					}
				}
				else{
					RampRecordedThread rt = new RampRecordedThread("tweets.txt", iStaticSend, iEvt, iTime);
					rt.start();
					rt.join();
					Thread.sleep(30000);
				}
			}
			if(env.equals("demo")){
				DemoRecordedThread th = new DemoRecordedThread("tweets.txt", 30,200,250);
				th.start();
				th.join();
				Thread.sleep(30000);
			}
			if(env.equals("real")){
				Scanner input = new Scanner(System.in);
				RealTimeThread ru1 = new RealTimeThread(Lists.newArrayList(Settings.real_keywords));
				ru1.start();
				String end = input.nextLine();
				ru1.kill();
				ru1.join();
				
				input.close();
			}
			
			
			System.out.println("closing...");
			System.exit(0);
		}
		
		
		
		
		if(Settings.state=="real"){
			Scanner input = new Scanner(System.in);
			RealTimeThread ru1 = new RealTimeThread(Lists.newArrayList(Settings.real_keywords));
			ru1.start();
			String end = input.nextLine();
			ru1.kill();
			ru1.join();
			
			System.out.println("closing...");
			input.close();
		}
		else if(Settings.state=="fake"){
			
			if(Settings.step){
				StepSyntheticThread ru2 = new StepSyntheticThread(400, 60);
				ru2.start();
				ru2.join();
				StepSyntheticThread ru3 = new StepSyntheticThread(2, 10);
				ru3.start();
				ru3.join();
				StepSyntheticThread ru4 = new StepSyntheticThread(200, 30);
				ru4.start();
				ru4.join();
			}
			else{
				RampSyntheticThread rt2 = new RampSyntheticThread(0, 1010, 120);
				rt2.start();
				rt2.join();
			}
		}
		else{
			if(Settings.step){
				StepRecordedThread ru3 = new StepRecordedThread("tweets.txt", 120, 250);
				ru3.start();
				ru3.join();
			}
			else{
				RampRecordedThread tr = new RampRecordedThread("tweets.txt", 0 , 480, 120);
				tr.start();
				tr.join();
			}
			
		}

		System.exit(0);
	}

}
