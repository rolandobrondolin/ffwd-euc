package it.polimi.necst.ffwd.producer;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONObject;

public class Serializer {

	public static byte[] serialize(Event obj) throws IOException {

		try(ByteArrayOutputStream b = new ByteArrayOutputStream()){
			try(DataOutputStream o = new DataOutputStream(b)){
				o.writeLong(obj.getTs());
				o.writeInt(obj.getKeywords().size());
				for(String s: obj.getKeywords()){
					o.writeUTF(s);
				}
				o.writeUTF(obj.getMessage().toString());
				o.writeUTF(obj.getText());
				o.writeInt(obj.getHashTags().size());
				for(String s: obj.getHashTags()){
					o.writeUTF(s);
				}
			}
			return b.toByteArray();
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return null;

	}

	public static Event deserialize(byte[] bytes) throws IOException, ClassNotFoundException {
		
		
		Event message = new Event();
		
		try(ByteArrayInputStream b = new ByteArrayInputStream(bytes)){
			try(DataInputStream o = new DataInputStream(b)){
				
				long ts = o.readLong();
				message.setTs(ts);
				
				int keySize = o.readInt();
				ArrayList<String> keywords = new ArrayList<>();
				for(int i=0; i<keySize; i++){
					keywords.add(o.readUTF());
				}
				message.setKeywords(keywords);
				
				String jsonString = o.readUTF();
				JSONObject msg = new JSONObject(jsonString);
				message.setMessage(msg);
				
				String text = o.readUTF();
				message.setText(text);
				
				int hashSize = o.readInt();
				ArrayList<String> hashtag = new ArrayList<>();
				for(int i=0; i<hashSize; i++){
					hashtag.add(o.readUTF());
				}
				message.setHashTags(hashtag);
				
				return message;
			}
		}
	}

}