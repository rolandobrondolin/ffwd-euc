package it.polimi.necst.ffwd.producer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeoutException;

import org.json.JSONArray;
import org.json.JSONObject;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;

import it.polimi.necst.ffwd.common.Settings;

public class DemoRecordedThread extends Thread{
	
	private String path;
	private String filename;
	int stable;
	int unstable1;
	int end;
	
	private Connection connection;
    Channel channel;
	
	public DemoRecordedThread(String filename, int stable, int unstable1, int end){
		this.filename = filename;
		this.stable = stable;
		this.unstable1 = unstable1;
		this.end = end;
	}
	
	@Override
	public void run(){
		path = System.getProperty("user.dir") + File.separator + filename;
		
		File f = new File(path);
		BufferedReader br = null;
		
		
		try {
			FileReader fir = new FileReader(f);
			br = new BufferedReader(fir);
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//Connect to rabbit queue
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("localhost");
		try {
			connection = factory.newConnection();
			channel = connection.createChannel();
			channel.queueDeclare(Settings.REAL_TIME_QUEUE, true, false, false, null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		
		
		//if buffered reader was opened correctly send tweets
		if(br!=null){
			//step 1: small arrival rate
			for(int i = 0; i<60; i++){
				long ts = System.currentTimeMillis();
				for(int j=0; j<stable; j++){
					try {
						
						JSONObject tw = null;
						do{
							String tweet = br.readLine();
							JSONObject tweetWrap = new JSONObject(tweet);
							tw = tweetWrap;
							
						}
						while(!tw.getJSONObject("_source").getString("lang").equals(Settings.tweetLanguage));
						
						JSONObject tweet = tw.getJSONObject("_source");
						String text = tweet.getString("text");
						ArrayList<String> hashtags = this.getHashTags(tweet);
						
						JSONArray keywords = tw.getJSONArray("keywords");
						ArrayList<String> keys = new ArrayList<String>();
						for(int k = 0; k< keywords.length(); k++){
							keys.add(keywords.getString(k));
						}
						
						Event msg = new Event(tweet);
						msg.setHashTags(hashtags);
						msg.setText(text);
						msg.setKeywords(keys);
						
						byte[] payload = Serializer.serialize(msg);
		    			if(payload!=null){
							channel.basicPublish("", Settings.REAL_TIME_QUEUE, MessageProperties.PERSISTENT_TEXT_PLAIN, payload);
		    			}
						
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				try {
					System.out.println(i + " " + 60 + " " + stable);
					long sleeptime = 1000 - (long) (((float)(System.currentTimeMillis() - ts))/1000);
					Thread.sleep(sleeptime);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			//step 2: high arrival rate
			for(int i = 0; i<180; i++){
				long ts = System.currentTimeMillis();
				for(int j=0; j<unstable1; j++){
					try {
						
						JSONObject tw = null;
						do{
							String tweet = br.readLine();
							JSONObject tweetWrap = new JSONObject(tweet);
							tw = tweetWrap;
							
						}
						while(!tw.getJSONObject("_source").getString("lang").equals(Settings.tweetLanguage));
						
						JSONObject tweet = tw.getJSONObject("_source");
						String text = tweet.getString("text");
						ArrayList<String> hashtags = this.getHashTags(tweet);
						
						JSONArray keywords = tw.getJSONArray("keywords");
						ArrayList<String> keys = new ArrayList<String>();
						for(int k = 0; k< keywords.length(); k++){
							keys.add(keywords.getString(k));
						}
						
						Event msg = new Event(tweet);
						msg.setHashTags(hashtags);
						msg.setText(text);
						msg.setKeywords(keys);
						
						byte[] payload = Serializer.serialize(msg);
		    			if(payload!=null){
							channel.basicPublish("", Settings.REAL_TIME_QUEUE, MessageProperties.PERSISTENT_TEXT_PLAIN, payload);
		    			}
						
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				try {
					System.out.println(i + " " + 180 + " " + unstable1);
					long sleeptime = 1000 - (long) (((float)(System.currentTimeMillis() - ts))/1000);
					Thread.sleep(sleeptime);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			//step 3: higher arrival rate
			for(int i = 0; i<60; i++){
				long ts = System.currentTimeMillis();
				for(int j=0; j<end; j++){
					try {
						
						JSONObject tw = null;
						do{
							String tweet = br.readLine();
							JSONObject tweetWrap = new JSONObject(tweet);
							tw = tweetWrap;
							
						}
						while(!tw.getJSONObject("_source").getString("lang").equals(Settings.tweetLanguage));
						
						JSONObject tweet = tw.getJSONObject("_source");
						String text = tweet.getString("text");
						ArrayList<String> hashtags = this.getHashTags(tweet);
						
						JSONArray keywords = tw.getJSONArray("keywords");
						ArrayList<String> keys = new ArrayList<String>();
						for(int k = 0; k< keywords.length(); k++){
							keys.add(keywords.getString(k));
						}
						
						Event msg = new Event(tweet);
						msg.setHashTags(hashtags);
						msg.setText(text);
						msg.setKeywords(keys);
						
						byte[] payload = Serializer.serialize(msg);
		    			if(payload!=null){
							channel.basicPublish("", Settings.REAL_TIME_QUEUE, MessageProperties.PERSISTENT_TEXT_PLAIN, payload);
		    			}
						
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				try {
					System.out.println(i + " " + 60 + " " + end);
					long sleeptime = 1000 - (long) (((float)(System.currentTimeMillis() - ts))/1000);
					Thread.sleep(sleeptime);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	

	private ArrayList<String> getHashTags(JSONObject tweet){
		try{
			ArrayList<String> tags = new ArrayList<String>();
			JSONArray hashtags = tweet.getJSONObject("entities").getJSONArray("hashtags");
			for(int i=0; i<hashtags.length(); i++){
				tags.add(hashtags.getJSONObject(i).getString("text"));
			}
			return tags;
		}
		catch (Exception e){
			return new ArrayList<String>();
		}
	}

}



