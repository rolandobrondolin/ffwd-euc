package it.polimi.necst.ffwd.producer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeoutException;

import org.json.JSONArray;
import org.json.JSONObject;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;

import it.polimi.necst.ffwd.common.Settings;

public class RampRecordedThread extends Thread{

	private int iterations;
	private int base;
	private int top;
	private String path;
	private String filename;

	
    private Connection connection;
    Channel channel;
	
	public RampRecordedThread(String filename, int base, int top, int iterations){
		this.base = base;
		this.iterations = iterations;
		this.top = top;
		this.filename = filename;
	}
	
	@Override
	public void run(){
		
		path = System.getProperty("user.dir") + File.separator + filename;
		
		File f = new File(path);
		BufferedReader br = null;
		
		
		try {
			FileReader fir = new FileReader(f);
			br = new BufferedReader(fir);
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("localhost");
		try {
			connection = factory.newConnection();
			channel = connection.createChannel();
			channel.queueDeclare(Settings.REAL_TIME_QUEUE, true, false, false, null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		
		int stepSize = (top - base)/iterations;

		//if buffered reader was opened correctly
		if(br!=null){
			
			for(int i = 1; i<= iterations; i++){
				long ts = System.currentTimeMillis();
				for(int j = base; j< (base + i*stepSize); j++){

					try {

						JSONObject tw = null;
						do{
							String tweet = br.readLine();
							JSONObject tweetWrap = new JSONObject(tweet);
							tw = tweetWrap.getJSONObject("_source");

						}
						while(!tw.getString("lang").equals(Settings.tweetLanguage));


						String text = tw.getString("text");
						ArrayList<String> hashtags = this.getHashTags(tw);

						Event msg = new Event(tw);
						msg.setHashTags(hashtags);
						msg.setText(text);

						byte[] payload = Serializer.serialize(msg);
						if(payload!=null){
							channel.basicPublish("", Settings.REAL_TIME_QUEUE, MessageProperties.PERSISTENT_TEXT_PLAIN, payload);
						}

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

				try {
					System.out.println(iterations + " " + i*stepSize);
					long sleeptime = 1000 - (long) (((float)(System.currentTimeMillis() - ts))/1000);
					Thread.sleep(sleeptime);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}
		
	}
	
	private ArrayList<String> getHashTags(JSONObject tweet){
		try{
			ArrayList<String> tags = new ArrayList<String>();
			JSONArray hashtags = tweet.getJSONObject("entities").getJSONArray("hashtags");
			for(int i=0; i<hashtags.length(); i++){
				tags.add(hashtags.getJSONObject(i).getString("text"));
			}
			return tags;
		}
		catch (Exception e){
			return new ArrayList<String>();
		}
	}
	
	
	
}
