package it.polimi.necst.ffwd.shedder;

public class FilterItem {
	public String key;
	public float pct;
	public int handler;
	
	public FilterItem(){
		key = "";
	}
	
	public FilterItem(String key, float pct, int handler){
		this.key = key;
		this.pct = pct;
		this.handler = handler;
	}
	
	
	
}
