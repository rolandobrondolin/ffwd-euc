package it.polimi.necst.ffwd.shedder;

import java.util.ArrayList;

import it.polimi.necst.ffwd.common.FileLogger;
import it.polimi.necst.ffwd.consumer.SentimentAggregator;
import it.polimi.necst.ffwd.producer.Event;

public class BaselinePolicy extends SheddingPolicy{

	public BaselinePolicy(SentimentAggregator sAggregator, LoadFilter filter, FileLogger logger) {
		super(sAggregator, filter, logger);
	}

	@Override
	public void computePlan(long capacity, long throughput, float n_evt, float qos) {
		
		//fair distribution among all the events
		float pct = (1 - (capacity/n_evt))*100;
		filter.setThreshold(pct, n_evt);
		
	}

	@Override
	public ArrayList<String> selectField(Event message) {
		return new ArrayList<String>();
	}

}
