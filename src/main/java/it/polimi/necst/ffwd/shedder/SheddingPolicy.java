package it.polimi.necst.ffwd.shedder;

import java.util.ArrayList;

import it.polimi.necst.ffwd.common.FileLogger;
import it.polimi.necst.ffwd.consumer.SentimentAggregator;
import it.polimi.necst.ffwd.producer.Event;

public abstract class SheddingPolicy {
	//batch queue definition
	public static final String BATCH_QUEUE = "batch_queue";
	protected SentimentAggregator sAggregator;
	protected LoadFilter filter;
	protected FileLogger logger;
	
	public SheddingPolicy(SentimentAggregator sAggregator, LoadFilter filter, FileLogger logger){
		this.sAggregator = sAggregator;
		this.filter = filter;
		this.logger = logger;
	}
	

	public abstract void computePlan(long capacity, long throughput, float n_evt, float targetResponseTime);
	
	public abstract ArrayList<String> selectField(Event message);
		
}
